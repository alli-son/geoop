import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <span>
      <Link to="/login">Login</Link>
    </span>
  );
};

export default Home;

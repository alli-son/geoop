import React, { Component } from "react";
import axios from "axios";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      error: ""
    };
  }

  change(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  submit(e) {
    e.preventDefault();
    axios
      .post("https://geoserviceuat-api.geo.ventures/oauth/token", null, {
        params: {
          grant_type: "password",
          login: this.state.username,
          password: this.state.password
        }
      })
      .then(res => {
        localStorage.setItem("token", res.data.access_token);
        this.props.history.push("/protected");
      });
  }

  render() {
    const { error } = this.state;
    return (
      <div>
        <form onSubmit={e => this.submit(e)}>
          <label>Username</label>
          <input type="text" name="username" onChange={e => this.change(e)} />
          <label>Password</label>
          <input
            type="password"
            name="password"
            onChange={e => this.change(e)}
          />
          <button type="submit">Submit</button>
        </form>
        {error && <p>Invalid credentials</p>}
      </div>
    );
  }
}

export default Login;

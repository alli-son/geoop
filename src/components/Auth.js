import React, { Component } from "react";
import { Link } from "react-router-dom";
import { withRouter } from "react-router-dom";
import { getJwt } from "../helpers";

class Auth extends Component {
  state = {
    users: []
  };
  getUsers = () => {
    const jwt = getJwt();
    if (!jwt) {
      this.setState({
        user: null
      });
      return;
    }
    fetch("https://geoserviceuat-api.geo.ventures/api/v1/job/actual", {
      headers: { Authorization: getJwt() }
    })
      .then(res => res.json())
      .then(data =>
        this.setState({
          users: data
        })
      );
  };

  render() {
    return (
      <div>
        <button onClick={this.getUsers}>Show Jobs</button>
        {this.state.users.map(user => {
          return (
            <div key={user.id}>
              <h2>Job Details</h2>
              <p>
                <b>Reference:</b> {user.reference}
              </p>
              <p>
                <b>Title:</b> {user.title}
              </p>
              <p>
                <b>Description:</b> {user.description}
              </p>
              <p>
                <b>Client Name:</b> {user.clients[0].first_name}{" "}
                {user.clients[0].last_name}
              </p>
              <p>
                <b>Company Name:</b> {user.clients[0].company_name}
              </p>
              <p>
                <b>Address:</b> {user.address.address1} {user.address.city}{" "}
                {user.address.state} {user.address.postcode}
              </p>
              <p>
                <b>Status:</b> {user.job_status.name}
              </p>
            </div>
          );
        })}
      </div>
    );
  }
}

export default withRouter(Auth);

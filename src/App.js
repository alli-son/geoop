import React, { Component } from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import Home from "./components/Home";
import Login from "./components/Login";
import Auth from "./components/Auth";
import Protected from "./components/Protected";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route path={"/"} exact component={Home} />
          <Route path={"/login"} component={Login} />
          <Auth>
            <Route path={"/protected"} component={Protected} />
          </Auth>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
